/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject;

/**
 *
 * @author a
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = 'N';
    
    public Robot(int x, int y, int bx, int by, int N){
        this.N = N;
        if(!inMap(x ,y)){
            this.x = 0;
            this.y = 0;
        }else{
            this.x = x;
            this.y = y;
        }
        if(!inMap(x ,y)){
            this.bx = 0;
            this.by = 0;
        }else{
            this.bx = bx;
            this.by = by;
        }
    }
    public boolean inMap(int x, int y){
        if(x>=N || x<0 || y>=N || y<0){
           return false;
        }
        return true;
    }
    // N:y-1 S:y+1 E:x+1 W:x-1     
    public boolean walk(char direction){
        switch(direction){
            case 'N':    
                if(!inMap(x, y-1)){
                    printUnableMove();
                    return false;
                }
                y = y-1;
                break;
            case 'S':
                if(!inMap(x, y+1)){
                    printUnableMove();
                    return false;
                }
                y = y+1;
                break;
            case 'E':
                if(!inMap(x+1, y)){
                    printUnableMove();
                    return false;
                }
                x = x+1;
                break;
            case 'W':
                if(!inMap(x-1, y)){
                    printUnableMove();
                    return false;
                }
                x = x-1;
                break;        
        }
        lastDirection = direction;
        if(isBomb()){
           printBomb();
        }
        return true;
    }
    
    public boolean walk(char direction, int step){
        for(int i = 0; i<step; i++){
            if(!this.walk(direction)){
                return false;
            }
        }
        return true;
    }
    
    public boolean walk(){
       return this.walk(lastDirection);
    }
    
    public boolean walk(int step){
       return this.walk(lastDirection, step);
    }
    
    public String toString(){
        return "Robot (" + this.x + ", " + this.y + ") " + " (" 
                + Math.abs(x-bx) +" , "+Math.abs(y-by) + ")";
    }
    public void printUnableMove(){
        System.out.println("I can't move!!!!");
    }
    
    public void printBomb(){
        System.out.println("Bomb found!!!");
    }
    
    public boolean isBomb(){
        if(x == bx && y == by){
            return true;
        }
        return false;
    } 
}
